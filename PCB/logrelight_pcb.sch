EESchema Schematic File Version 4
LIBS:logrelight_pcb-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L logrelight_pcb-rescue:BC547-Transistor_BJT Q1
U 1 1 5BC6540F
P 4800 4600
F 0 "Q1" H 4991 4646 50  0000 L CNN
F 1 "BC547" H 4991 4555 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_HandSolder" H 5000 4525 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 4800 4600 50  0001 L CNN
	1    4800 4600
	1    0    0    -1  
$EndComp
$Comp
L logrelight_pcb-rescue:R-Device R1
U 1 1 5BC65506
P 4100 4250
F 0 "R1" H 4170 4296 50  0000 L CNN
F 1 "100" H 4170 4205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4030 4250 50  0001 C CNN
F 3 "~" H 4100 4250 50  0001 C CNN
	1    4100 4250
	1    0    0    -1  
$EndComp
$Comp
L logrelight_pcb-rescue:LED-Device D1
U 1 1 5BC78337
P 5500 4450
F 0 "D1" V 5538 4333 50  0000 R CNN
F 1 "LED" V 5447 4333 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm_Horizontal_O1.27mm_Z15.0mm" H 5500 4450 50  0001 C CNN
F 3 "~" H 5500 4450 50  0001 C CNN
	1    5500 4450
	0    -1   -1   0   
$EndComp
$Comp
L logrelight_pcb-rescue:GND-power #PWR0101
U 1 1 5BC784C0
P 4900 5000
F 0 "#PWR0101" H 4900 4750 50  0001 C CNN
F 1 "GND" H 4905 4827 50  0000 C CNN
F 2 "" H 4900 5000 50  0001 C CNN
F 3 "" H 4900 5000 50  0001 C CNN
	1    4900 5000
	1    0    0    -1  
$EndComp
$Comp
L logrelight_pcb-rescue:Battery-Device BT1
U 1 1 5BC78692
P 3150 3950
F 0 "BT1" H 3258 3996 50  0000 L CNN
F 1 "Battery" H 3258 3905 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" V 3150 4010 50  0001 C CNN
F 3 "~" V 3150 4010 50  0001 C CNN
	1    3150 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3750 3150 3450
Wire Wire Line
	4100 4400 4100 4600
Wire Wire Line
	4100 4600 4600 4600
Wire Wire Line
	4900 4400 4900 4100
Wire Wire Line
	4900 5000 4900 4925
Wire Wire Line
	4900 4925 3150 4925
Wire Wire Line
	5500 4300 5500 4100
Wire Wire Line
	5500 4100 4900 4100
Wire Wire Line
	4900 4925 5500 4925
Connection ~ 4900 4925
Wire Wire Line
	3150 4150 3150 4925
Wire Wire Line
	4900 4800 4900 4925
Wire Wire Line
	5500 4600 5500 4925
$Comp
L logrelight_pcb-rescue:INDUCTOR-pspice L1
U 1 1 5BC78DF0
P 4100 3775
F 0 "L1" V 4054 3853 50  0000 L CNN
F 1 "INDUCTOR" V 4145 3853 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x01_P2.54mm_Vertical" H 4100 3775 50  0001 C CNN
F 3 "~" H 4100 3775 50  0001 C CNN
	1    4100 3775
	0    1    1    0   
$EndComp
Wire Wire Line
	3150 3450 3350 3450
Wire Wire Line
	4100 3525 4100 3450
Connection ~ 4100 3450
Wire Wire Line
	4100 3450 4900 3450
Wire Wire Line
	4900 3550 4900 3450
Wire Wire Line
	4100 4025 4100 4100
$Comp
L logrelight_pcb-rescue:INDUCTOR-pspice L2
U 1 1 5BC78ECC
P 4900 3800
F 0 "L2" V 4854 3878 50  0000 L CNN
F 1 "INDUCTOR" V 4945 3878 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x01_P2.54mm_Vertical" H 4900 3800 50  0001 C CNN
F 3 "~" H 4900 3800 50  0001 C CNN
	1    4900 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	4900 4100 4900 4050
Connection ~ 4900 4100
$Comp
L logrelight_pcb-rescue:SW_DIP_x01-Switch SW1
U 1 1 5DA0D358
P 3650 3450
F 0 "SW1" H 3650 3717 50  0000 C CNN
F 1 "SW_DIP_x01" H 3650 3626 50  0000 C CNN
F 2 "" H 3650 3450 50  0001 C CNN
F 3 "" H 3650 3450 50  0001 C CNN
	1    3650 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3450 4100 3450
Text Label 3925 4925 0    50   ~ 0
gnd
Text Label 4250 4600 0    50   ~ 0
base
Text Label 5175 4100 0    50   ~ 0
Anode
Text Label 4100 4075 0    50   ~ 0
Res
Text Label 4400 3450 0    50   ~ 0
switch
Text Label 3225 3450 0    50   ~ 0
bat
$EndSCHEMATC
